//
//  MainViewController.m
//  Qianlb-iOS
//
//  Created by 杨卢银 on 16/9/21.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self performSelector:@selector(loadStoryBoard) withObject:self afterDelay:2];
    
}


-(void)loadStoryBoard{
    BOOL isfirst = [[[NSUserDefaults standardUserDefaults] objectForKey:@"GOG_UNISFIRST"] boolValue];
    if (isfirst==NO) {
        [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"GOG_UNISFIRST"];
    }else{
    
    }
    [self presentSBRootViewController:K_SB_Main];
}
    
    
@end
