//
//  DZTViewController.h
//  Qianlb-iOS
//
//  Created by 杨卢银 on 16/9/21.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>


#define K_SB_Main          @"DZTMain"
#define K_SB_Home          @"DZTHome"
#define K_SB_Zone          @"DZTZone"
#define K_SB_ShopCar       @"DZTShopCar"
#define K_SB_More          @"DZTMore"

@interface DZTViewController : UIViewController

-(UIViewController*)getViewController:(NSString*)className StroyBoard:(NSString*)sb_name;

-(void)pushViewController:(UIViewController*)viewController;


-(void)presentViewController:(UIViewController*)viewController;

-(void)presentSBRootViewController:(NSString*)sb_name;

-(void)presentSB:(NSString*)sb_name ViewController:(UIViewController*)viewController;


-(void)popToRootViewControllerAnimated;

#pragma mark Ipad
-(void)hd_presentViewController:(UIViewController*)viewcontroller;

-(void)hd_presentViewController:(UIViewController*)viewcontroller contentSize:(CGSize)size landscapeContentSize:(CGSize)landscapeContentSize;



-(void)hd_pushViewController:(UIViewController *)viewcontroller;
/**
 *  返回上一个VC
 */
-(void)hd_popViewController;
-(void)popViewController;




- (void)dismissKeyBroad;

- (void) addLeftButtonTitle:(NSString*)title select:(void(^)(DZTViewController *viewcontroller,UIButton *item))callback;

- (void) addRightButtonTitle:(NSString*)title select:(void(^)(DZTViewController *viewcontroller,UIButton *item))callback;

- (void) addLeftButtonImage:(NSString*)imageName select:(void(^)(DZTViewController *viewcontroller,UIButton *item))callback;

- (void) addRightButtonImage:(NSString*)imageName select:(void(^)(DZTViewController *viewcontroller,UIButton *item))callback;

/**
 *通过View 找到indexPath
 */
-(NSIndexPath*)indexsPathFromView:(UIView*)view byTableView:(UITableView*)tableView;

@end
