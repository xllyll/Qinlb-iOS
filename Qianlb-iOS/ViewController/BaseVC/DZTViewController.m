//
//  DZTViewController.m
//  Qianlb-iOS
//
//  Created by 杨卢银 on 16/9/21.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "DZTViewController.h"

@interface DZTViewController ()

{
    void (^leftButtonSelect)(DZTViewController *viewcontroller,UIButton*);
    void (^rightButtonSelect)(DZTViewController *viewcontroller,UIButton*);
}
@end

@implementation DZTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - 推出虚拟键盘
- (void)dismissKeyBroad{
    for (UITextField *txt in self.view.subviews) {
        if ([txt isMemberOfClass:[UITextField class]]) {
            
            [txt resignFirstResponder];
        }
    }
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismissKeyBorad];
}

-(void)dismissKeyBorad{
    for (UITextField *txt in self.view.subviews) {
        if ([txt isMemberOfClass:[UITextField class]]) {
            
            [txt resignFirstResponder];
        }
    }
}



-(UIViewController *)getViewController:(NSString *)className StroyBoard:(NSString *)sb_name{
    UIViewController *vc = [[UIStoryboard storyboardWithName:sb_name bundle:nil] instantiateViewControllerWithIdentifier:className];
    return vc;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)pushViewController:(UIViewController *)viewController{
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)presentViewController:(UIViewController *)viewController{
    self.hidesBottomBarWhenPushed = YES;
    [self presentViewController:viewController animated:YES completion:^{
        
    }];
}

-(void)presentSB:(NSString *)sb_name ViewController:(UIViewController *)viewController{
    
}


-(void)presentSBRootViewController:(NSString *)sb_name{
    
    UIViewController *vc = [[UIStoryboard storyboardWithName:sb_name bundle:nil] instantiateInitialViewController];
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}
-(void)popToRootViewControllerAnimated{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark Ipad
-(void)hd_presentViewController:(UIViewController *)viewcontroller{
    
    //    viewcontroller.contentSizeInPopup = CGSizeMake(600, 600);
    //    viewcontroller.landscapeContentSizeInPopup = CGSizeMake(600, 600);
    //    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:viewcontroller];
    //    popupController.containerView.layer.cornerRadius = 5;
    //    popupController.transitionStyle = STPopupTransitionStyleFade;
    //    [popupController presentInViewController:self];
}
-(void)hd_pushViewController:(UIViewController *)viewcontroller{
    //    viewcontroller.contentSizeInPopup = CGSizeMake(600, 600);
    //    viewcontroller.landscapeContentSizeInPopup = CGSizeMake(600, 600);
    //    [self.popupController pushViewController:viewcontroller animated:YES];
    
}
-(void)hd_presentViewController:(UIViewController *)viewcontroller contentSize:(CGSize)size landscapeContentSize:(CGSize)landscapeContentSize{
    //    viewcontroller.contentSizeInPopup = CGSizeMake(600, 600);
    //    viewcontroller.landscapeContentSizeInPopup = CGSizeMake(600, 600);
    //    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:viewcontroller];
    //    popupController.containerView.layer.cornerRadius = 5;
    //    popupController.transitionStyle = STPopupTransitionStyleFade;
    //    [popupController presentInViewController:self];
}



-(void)hd_popViewController{
    //    [self.popupController popViewControllerAnimated:YES];
}
-(void)popViewController{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark nave

-(void)leftButtonSelect:(UIButton*)sender{
    if (leftButtonSelect) {
        (leftButtonSelect)(self,sender);
    }
}
-(void)rightButtonSelect:(UIButton*)sender{
    if (rightButtonSelect) {
        (rightButtonSelect)(self,sender);
    }
}

-(void)addLeftButtonTitle:(NSString *)title select:(void (^)(DZTViewController *viewcontroller,UIButton *))callback{
    leftButtonSelect = callback;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    btn.titleLabel.font  = [UIFont systemFontOfSize:15.0];
    
    [btn setFrame:CGRectMake(0.0f, 0.0f, 60.0f, 30.0f)];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(leftButtonSelect:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *dict = @{NSFontAttributeName: btn.titleLabel.font};
    CGSize textSize = [title boundingRectWithSize:CGSizeMake(100, 30)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:dict
                                          context:nil].size;
    if (textSize.width>30) {
        [btn setFrame:CGRectMake(0.0f, 0.0f, textSize.width, 30.0f)];
    }
    //最后就是把创建的按钮添加到navigationbar上
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
}
-(void)addRightButtonTitle:(NSString *)title select:(void (^)(DZTViewController *viewcontroller,UIButton *))callback{
    if (title==nil) {
        self.navigationItem.rightBarButtonItem = nil;
        return;
    }
    rightButtonSelect = callback;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font  = [UIFont systemFontOfSize:15.0];
    [btn setFrame:CGRectMake(0.0f, 0.0f, 60.0f, 30.0f)];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(rightButtonSelect:) forControlEvents:UIControlEventTouchUpInside];
    NSDictionary *dict = @{NSFontAttributeName: btn.titleLabel.font};
    CGSize textSize = [title boundingRectWithSize:CGSizeMake(100, 30)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:dict
                                          context:nil].size;
    if (textSize.width>30) {
        [btn setFrame:CGRectMake(0.0f, 0.0f, textSize.width, 30.0f)];
    }
    //最后就是把创建的按钮添加到navigationbar上
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(rightButtonSelect:)];
}
-(void)addLeftButtonImage:(NSString *)imageName select:(void (^)(DZTViewController *viewcontroller,UIButton *))callback{
    leftButtonSelect = callback;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(leftButtonSelect:) forControlEvents:UIControlEventTouchUpInside];
    //最后就是把创建的按钮添加到navigationbar上
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
}

-(void)addRightButtonImage:(NSString *)imageName select:(void (^)(DZTViewController *viewcontroller,UIButton *))callback{
    rightButtonSelect = callback;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(rightButtonSelect:) forControlEvents:UIControlEventTouchUpInside];
    //最后就是把创建的按钮添加到navigationbar上
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
}

/**
 *通过View 找到indexPath
 */
-(NSIndexPath*)indexsPathFromView:(UIView*)view byTableView:(UITableView*)tableView
{
    UIView *parentView = [view superview];
    
    while (![parentView isKindOfClass:[UITableViewCell class]] && parentView!=nil) {
        parentView = parentView.superview;
    }
    NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell*)parentView];
    
    return indexPath;
}
@end
