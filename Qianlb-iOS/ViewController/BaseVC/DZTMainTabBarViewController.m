//
//  DZTMainTabBarViewController.m
//  Qianlb-iOS
//
//  Created by 杨卢银 on 16/9/21.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "DZTMainTabBarViewController.h"
#import "DZTHomeViewController.h"
#import "DZTZoneViewController.h"
#import "DZTShopCarViewController.h"
#import "DZTMoreViewController.h"


@interface DZTMainTabBarViewController ()
{
    
    DZTHomeViewController    *_homeVC;
    DZTZoneViewController    *_zoneVC;
    DZTShopCarViewController *_shopCarVC;
    DZTMoreViewController    *_moreVC;
    
}
@end

@implementation DZTMainTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self buildControllers];
}

- (void)buildControllers{
    
    _homeVC = [self getController:@"DZTHomeViewController" sbName:K_SB_Home];
    UINavigationController *homeNav = [[UINavigationController alloc]initWithRootViewController:_homeVC];
    homeNav.title = @"首页";
    
    _zoneVC = [self getController:@"DZTZoneViewController" sbName:K_SB_Zone];
    UINavigationController *zoneNav = [[UINavigationController alloc]initWithRootViewController:_zoneVC];
    zoneNav.title = @"首页";
    
    _shopCarVC = [self getController:@"DZTShopCarViewController" sbName:K_SB_ShopCar];
    UINavigationController *shopcarNav = [[UINavigationController alloc]initWithRootViewController:_shopCarVC];
    shopcarNav.title = @"购物车";
    
    _moreVC = [self getController:@"DZTMoreViewController" sbName:K_SB_More];
    UINavigationController *moreNav = [[UINavigationController alloc]initWithRootViewController:_moreVC];
    moreNav.title = @"我";
    
    
    self.viewControllers = @[homeNav,zoneNav,shopcarNav,moreNav];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (id)getController:(NSString*)className sbName:(NSString*)sbName{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:sbName bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:className];
    return vc;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
