//
//  XYUtils.m
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "XYUtils.h"

//Get IP 需要导入的库文件

#import <ifaddrs.h>

#import <arpa/inet.h>


//Get MAC 需要导入的库文件

#include <sys/socket.h> // Per msqr

#include <sys/sysctl.h>

#include <net/if.h>

#include <net/if_dl.h>

#include <CommonCrypto/CommonDigest.h>

@implementation XYUtils

#pragma mark FUC

/**
 *  MD5加密（16位）
 *
 *  @param string 字符串
 *
 *  @return md5加密字符串
 */
+(NSString *)md5:(NSString *)string
{
    //NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    //提取32位MD5散列的中间16位
    NSString *md5_32Bit_String=[self getMd5_32Bit_String:string];
    NSString *result = [[md5_32Bit_String substringToIndex:24] substringFromIndex:8];//即9～25位
    
    return result;
}

+(NSString *)md5_32:(NSString *)string{
    NSString *md5_32Bit_String=[self getMd5_32Bit_String:string];
    return md5_32Bit_String;
}
+(NSString *)getMd5_32Bit_String:(NSString *)srcString{
    const char *cStr = [srcString UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), digest );
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH *2];
    for(int i =0; i < CC_MD5_DIGEST_LENGTH; i++)
        [result appendFormat:@"%02x", digest[i]];
    
    return result;
}


#pragma mark IP
/**
 *  @Author, 15-03-24 09:07:06
 *
 *  Get IP Address
 *
 *  #import <ifaddrs.h>
 *
 *  #import <arpa/inet.h>
 *
 */

+ (NSString *)getIPAddress {
    
    NSString *address = @"error";
    
    struct ifaddrs *interfaces = NULL;
    
    struct ifaddrs *temp_addr = NULL;
    
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    
    success = getifaddrs(&interfaces);
    
    if (success == 0) {
        
        // Loop through linked list of interfaces
        
        temp_addr = interfaces;
        
        while(temp_addr != NULL) {
            
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                
                // Check if interface is en0 which is the wifi connection on the iPhone
                
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    
                    // Get NSString from C String
                    
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
        
    }
    
    // Free memory
    
    freeifaddrs(interfaces);
    
    return address;
}

#pragma mark MAC

/**
 
 *  @Author , 15-03-24 09:07:06
 
 *
 
 *   #include <sys/socket.h> // Per msqr
 
 *   #include <sys/sysctl.h>
 
 *   #include <net/if.h>
 
 *   #include <net/if_dl.h>
 
 *
 
 *   Return the local MAC addy
 
 *   Courtesy of FreeBSD hackers email list
 
 *   Accidentally munged during previous update. Fixed thanks to mlamb.
 
 *
 
 */

+ (NSString *) getMacAddress
{
    int mib[6];
    
    size_t len;
    
    char *buf;
    
    unsigned char *ptr;
    
    struct if_msghdr *ifm;
    
    struct sockaddr_dl *sdl;
    
    
    mib[0] = CTL_NET;
    
    mib[1] = AF_ROUTE;
    
    mib[2] = 0;
    
    mib[3] = AF_LINK;
    
    mib[4] = NET_RT_IFLIST;
    
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        
        printf("Error: if_nametoindex error/n");
        
        return NULL;
        
    }
    
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        
        printf("Error: sysctl, take 1/n");
        
        return NULL;
        
    }
    
    if ((buf = malloc(len)) == NULL) {
        
        printf("Could not allocate memory. error!/n");
        
        return NULL;
        
    }
    
    
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        
        printf("Error: sysctl, take 2");
        
        return NULL;
        
    }
    
    
    ifm = (struct if_msghdr *)buf;
    
    sdl = (struct sockaddr_dl *)(ifm + 1);
    
    ptr = (unsigned char *)LLADDR(sdl);
    
    NSString *outstring = [NSString stringWithFormat:@"%02x:%02x:%02x:%02x:%02x:%02x", *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    
    free(buf);
    
    return [outstring uppercaseString];
    
}

@end
