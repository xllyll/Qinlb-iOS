//
//  XYFileUtils.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/14.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 *  文件保存
 */
@interface XYFileUtils : NSObject
/**
 *  保存数据到PLIST文件中
 *
 *  @param dictionary 数据
 *  @param fileName   文件名称
 *
 */
+(BOOL)savePlistFile:(id)dictionary fileName:(NSString*)fileName;

/**
 *  获取PLIST数据
 *
 *  @param fileName 文件名称
 *
 */
+(id)getPlistFileByFileName:(NSString*)fileName;

@end
