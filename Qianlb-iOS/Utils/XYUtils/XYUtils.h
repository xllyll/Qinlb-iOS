//
//  XYUtils.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XYUtils : NSObject

+(NSString *)md5:(NSString *)str;

+(NSString *)md5_32:(NSString*)string;

+ (NSString *) getIPAddress;

+ (NSString *) getMacAddress;

@end
