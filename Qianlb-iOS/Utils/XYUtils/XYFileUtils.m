//
//  XYFileUtils.m
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/14.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "XYFileUtils.h"

#define XY_Documents_Path [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

@implementation XYFileUtils
+(NSString*)getPath:(NSString*)p filename:(NSString*)filename{
    
    NSString *path = [NSString stringWithFormat:@"%@/GOGDATA/%@/",XY_Documents_Path,p];
    
    NSArray *sarray = [filename componentsSeparatedByString:@"/"];
    NSString *s = filename;
    if (sarray && sarray.count>1) {
        for (int i = 0; i < sarray.count-1;i++) {
            path = [NSString stringWithFormat:@"%@/%@",path,sarray[i]];
        }
        s = [sarray lastObject];
    }
    BOOL bo = [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    NSAssert(bo,@"创建目录失败");
    if (bo==YES) {
        
    }
    return [[path stringByAppendingString:@"/"] stringByAppendingString:s];
}
+(BOOL)savePlistFile:(id)dictionary fileName:(NSString*)fileName{
    
    NSString *str = [NSString stringWithFormat:@"%@.plist",fileName];
    
    NSString *filePath=[self getPath:@"FILE/JsonFile" filename:str];   //获取路径
    
    XYLog(@"文件位置：\n%@",filePath);

    BOOL isok =  [dictionary writeToFile:filePath atomically:YES];
    
    return isok;
}
+(id)getPlistFileByFileName:(NSString *)fileName{
    
    NSString *str = [NSString stringWithFormat:@"%@.plist",fileName];
    
    NSString *filePath=[self getPath:@"File/JsonFile" filename:str];   //获取路径
    //读文件
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    return dictionary;
}

@end
