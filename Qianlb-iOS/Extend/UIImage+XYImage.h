//
//  UIImage+XYImage.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>


#define XYImageByName(value)  [UIImage imageNamed:value]

@interface UIImage(XYImage)


/**
 *  保存图片到缓存路径中
 *
 *  @param image           图片
 *  @param image_name      图片链接
 *  @param foundation_name 子文件夹名称
 *
 */
+(BOOL)xy_saveImage:(UIImage *)image imageName:(NSString *)image_name foundation:(NSString *)foundation_name;

/**
 *  得到缓存的图片
 *
 *  @param image_name      图片名称
 *  @param foundation_name 图片子文件夹名称
 *
 *  @return UIimage
 */
+(UIImage *)xy_getImageName:(NSString *)image_name foundation:(NSString *)foundation_name;

/**
 *  将颜色转换成图片 UIColor-->UIImage
 *
 *  @param color 颜色 UIColor
 *
 *  @return UIImage
 */
+(UIImage*)xy_createImageWithColor:(UIColor*) color;

@end
