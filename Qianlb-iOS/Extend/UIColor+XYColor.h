//
//  UIColor+XYColor.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/9.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>

#define XYRGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]


@interface UIColor(XYColor)

@end
