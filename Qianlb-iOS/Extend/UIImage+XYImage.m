//
//  UIImage+XYImage.m
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "UIImage+XYImage.h"
#import "NSString+XYString.h"

#define XY_PATH_OF_CACHE       [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]

@implementation UIImage(XYImage)

+(BOOL)xy_saveImage:(UIImage *)image imageName:(NSString *)image_name foundation:(NSString *)foundation_name{
    
    
    
    if(image_name==nil || [image_name isKindOfClass:[NSNull class]]){
        return NO;
    }
    if (foundation_name==nil) {
        foundation_name = @"CACHE";
    }
    NSString *path = [[[XY_PATH_OF_CACHE stringByAppendingPathComponent:@"XYDATA"] stringByAppendingPathComponent:@"Image"] stringByAppendingPathComponent:foundation_name];
    BOOL bo = [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    NSAssert(bo,@"创建目录失败");
    if (bo==YES) {
        
    }
    NSString *uniquePath=[XY_PATH_OF_CACHE stringByAppendingPathComponent:[NSString stringWithFormat:@"/XYDATA/Image/%@/image_%@.png",foundation_name,[image_name XY_MD5String]]];//以model.itemid image.png为区分保存在Documents中
    BOOL result = [UIImagePNGRepresentation(image) writeToFile:uniquePath atomically:YES];
    return result;
}

+(UIImage *)xy_getImageName:(NSString *)image_name foundation:(NSString *)foundation_name{
    if(image_name==nil || [image_name isKindOfClass:[NSNull class]]){
        return nil;
    }
    NSString *uniquePath=[XY_PATH_OF_CACHE stringByAppendingPathComponent:[NSString stringWithFormat:@"/XYDATA/Image/%@/image_%@.png",foundation_name,[image_name XY_MD5String]]];
    UIImage *image = [[UIImage alloc]initWithContentsOfFile:uniquePath];
    return image;
}

//UIColor 转UIImage
+(UIImage*) xy_createImageWithColor: (UIColor*) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

@end
