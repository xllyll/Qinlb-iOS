//
//  UIImageView+XYImageView.h
//  XLLYLL-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+XYImage.h"

@interface UIImageView(XYImageView)

/**
 *  设置图片
 *
 *  @param imageUrlString 图片链接
 *  @param placeimage     默认图片
 */
- (void)xy_setImageURL:(NSString*)imageUrlString placeholderImage:(UIImage *)placeimage;

@end
