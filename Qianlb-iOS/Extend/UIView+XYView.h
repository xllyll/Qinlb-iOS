//
//  UIView+XYView.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFont+XYFont.h"
#import "UIImageView+XYImageView.h"

#define XY_WINDOW      [UIScreen mainScreen].bounds
#define XY_WINDOW_SIZE [UIScreen mainScreen].bounds.size

typedef NS_ENUM(NSInteger, XYViewAnimationType) {
    XYViewAnimationTypeNone,/**< 默认从0开始 */
    XYViewAnimationTypeShake,/**< 抖动动画 */
};
@interface UIView(XYView)

/**
 *视图宽度
 */
@property (nonatomic,assign) CGFloat width;
/**
 *视图高度
 */
@property (nonatomic,assign) CGFloat height;

/**
 *视图X坐标
 */
@property (nonatomic,assign) CGFloat x;
/**
 *视图Y坐标
 */
@property (nonatomic,assign) CGFloat y;

/**
 *视图X坐标（最大）
 */
@property (nonatomic,assign) CGFloat maxX;
/**
 *视图Y坐标 （最大）
 */
@property (nonatomic,assign) CGFloat maxY;


/**
 * 小红点
 */
@property (nonatomic,copy) NSString *badgeString;


/**
 *设置为圆形视图
 */
-(void)setRoundView;

/**
 *设置为圆角视图<angle 角度>
 */
-(void)setRoundViewByAngle:(float)angle;

/**
 * 设置view 的 边线
 */
-(void)setBorderWidth:(CGFloat)width borderColor:(UIColor*)color;


/**
 *设置为圆形视图
 */
-(void)setAnimationType:(XYViewAnimationType)animationType;

/**
 *  关闭键盘
 */
- (void)dismissKeyboard;

@end
