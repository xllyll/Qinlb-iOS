//
//  UIColor+GOGColor.m
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/9.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "UIColor+GOGColor.h"

@implementation UIColor(GOGColor)

+(UIColor *)gog_redColor{
    
    return XYRGBCOLOR(211, 24, 14);
}
+(UIColor *)gog_lineColor{
    
    return XYRGBCOLOR(179, 179, 179);
}
@end
