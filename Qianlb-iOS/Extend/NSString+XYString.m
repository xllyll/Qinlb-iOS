//
//  NSString+XYString.m
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "NSString+XYString.h"
#import "XYUtils.h"

@implementation NSString(XYString)

-(NSString *)XY_MD5String{
    
    return [XYUtils md5:self];
    
}
-(NSString *)XY_MD5_32_String{
    
    return [XYUtils md5_32:self];
}
-(NSString *)XY_DesString{
    return @"";
    //return [FUDesUtils encryptUseDES:self];
}
+(NSString *)LocalizedString:(NSString *)tineString{
    
    NSString *string = NSLocalizedStringFromTableInBundle(tineString, @"Localizable", [NSBundle mainBundle], nil);
    
    return string;
}




@end
