//
//  NSString+XYString.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(XYString)

/**
 *  MD5 加密字符串
 *
 *  @return MD5
 */
-(NSString *)XY_MD5String;
/**
 *  MD5 加密字符串(32)
 *
 *  @return MD5
 */
-(NSString *)XY_MD5_32_String;
/**
 *  DES 加密
 *
 *  @return Des
 */
-(NSString *)XY_DesString;

//
+(NSString *)LocalizedString:(NSString*)tineString;

@end
