//
//  UIFont+XYFont.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/12.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>

/**< 一般字体 */
#define XYFont(value)  [UIFont systemFontOfSize:value] /**< 一般字体 */
/**< 加粗字体 */
#define XYBoldFont(value)  [UIFont boldSystemFontOfSize:value] /**< 加粗字体 */

@interface UIFont(XYFont)

@end
