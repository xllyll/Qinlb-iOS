//
//  UIImageView+XYImageView.m
//  XLLYLL-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "UIImageView+XYImageView.h"
#import "UIImage+XYImage.h"

@implementation UIImageView(XYImageView)

-(void)xy_setImageURL:(NSString *)imageUrlString placeholderImage:(UIImage *)placeimage{
    self.image = placeimage;
    
    UIImage *timage = [UIImage xy_getImageName:imageUrlString foundation:@"CACHE"];
    if (timage) {
        self.image = timage;
    }else{
        if(imageUrlString ==nil || [imageUrlString isKindOfClass:[NSNull class]] || imageUrlString.length==0){
            return;
        }
        
//        GOGHttpAction *action = [[GOGHttpAction alloc]init];
//        [action download:imageUrlString progressBlock:^(CGFloat progress) {
//            
//        } ResponseData:^(NSData *i_data, NSError *error) {
//            if (i_data) {
//                UIImage *tempImage = [UIImage imageWithData:i_data];
//                if (tempImage) {
//                    [UIImage xy_saveImage:tempImage imageName:imageUrlString foundation:@"CACHE"];
//                    self.image = tempImage;
//                }else{
//                    XYLog(@"data图片下载失败！");
//                }
//            }else{
//                XYLog(@"图片下载失败！");
//            }
//        }];
        
        
        NSURL *url = [NSURL URLWithString:imageUrlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        if (data) {
            UIImage *tempImage = [UIImage imageWithData:data];
            if (tempImage) {

                [UIImage xy_saveImage:tempImage imageName:imageUrlString foundation:@"CACHE"];
                self.image = tempImage;
            }else{
                XYLog(@"data图片下载失败！");
            }
        }else{
            XYLog(@"图片下载失败！");
        }
        }];
    }
}

@end
