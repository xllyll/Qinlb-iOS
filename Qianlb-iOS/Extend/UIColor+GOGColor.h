//
//  UIColor+GOGColor.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/9.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+XYColor.h"

@interface UIColor(GOGColor)

+(UIColor*)gog_redColor;

+(UIColor*)gog_lineColor;

@end
