//
//  XYGraphCodeView.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/13.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XYGraphCodeView;

@protocol XYGraphCodeViewDelegate <NSObject>

//点击图形验证码
- (void)didTapGraphCodeView:(XYGraphCodeView *)graphCodeView;

@end

@interface XYGraphCodeView : UIView

@property (nonatomic,strong) NSString *code;//接收外部传的code

@property (nonatomic,assign) id<XYGraphCodeViewDelegate> delegate;

@end