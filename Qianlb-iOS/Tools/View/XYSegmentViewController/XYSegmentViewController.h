//
//  XYSegmentViewController.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/19.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYSegmentViewController : UIViewController

@property (strong , nonatomic) NSArray *segmentViewControllers;

@end
