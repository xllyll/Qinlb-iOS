//
//  XYSegmentViewController.m
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/19.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "XYSegmentViewController.h"

#define K_Top_SegmentView_Height 36

@interface XYSegmentViewController ()

@property (strong , nonatomic) UIControl *topSegmentMenuControl;

@property (strong , nonatomic) UIControl *sliderView;

@property (strong , nonatomic) UIScrollView *scrollView;

@end

@implementation XYSegmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSegmentViewUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initSegmentViewUI{
    [self buildSegmentMenu];
}
-(void)buildSegmentMenu{
    
    [self.view addSubview:self.topSegmentMenuControl];

    
}

-(UIControl *)topSegmentMenuControl{
    if (_topSegmentMenuControl==nil) {
        
        _topSegmentMenuControl = [[UIControl alloc]initWithFrame:CGRectMake(0, 64, XY_WINDOW_SIZE.width, K_Top_SegmentView_Height)];
        _topSegmentMenuControl.backgroundColor = [UIColor whiteColor];
    }
    return _topSegmentMenuControl;
}
-(void)setSegmentViewControllers:(NSArray *)segmentViewControllers{
    _segmentViewControllers = segmentViewControllers;
    
    [self buildSubController];
}

-(void)buildSubController{
    if(_segmentViewControllers){
        float p = 1.0/(float)_segmentViewControllers.count;
        float c_w = self.topSegmentMenuControl.width*p;
        float c_h = self.topSegmentMenuControl.height-2;
        
        if (_scrollView==nil) {
            _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _topSegmentMenuControl.maxY, _topSegmentMenuControl.width, self.view.height-_topSegmentMenuControl.maxY)];
            [self.view addSubview:_scrollView];
        }
        
        [_scrollView setContentSize:CGSizeMake(_scrollView.width*_segmentViewControllers.count, _scrollView.height)];
        [_scrollView setPagingEnabled:YES];
        
        for (int i = 0 ; i < _segmentViewControllers.count; i++) {
            
            UIViewController *vc = _segmentViewControllers[i];
            
            UIControl *control = [[UIControl alloc]initWithFrame:CGRectMake(c_w*i, 0, c_w, c_h)];
            control.backgroundColor = [UIColor clearColor];
            [self.topSegmentMenuControl addSubview:control];
            
            UILabel *label = [[UILabel alloc]init];
            label.font = [UIFont systemFontOfSize:15.0];
            label.text = vc.title;
            label.textColor = [UIColor redColor];
            [label sizeToFit];
            [control addSubview:label];
            label.center = CGPointMake(control.width/2.0, control.height/2.0);
            if (i==0) {
                if (_sliderView==nil) {
                    _sliderView = [[UIControl alloc]initWithFrame:CGRectMake(label.x, control.maxY, label.width, 2)];
                    _sliderView.backgroundColor = [UIColor redColor];
                    [self.topSegmentMenuControl addSubview:_sliderView];
                }
                _sliderView.frame = CGRectMake(label.x, control.maxY, label.width, 2);
            }
            vc.view.frame = CGRectMake(_scrollView.width*i, 0, _scrollView.width, _scrollView.height);
            [_scrollView addSubview:vc.view];
            
        }
        
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
