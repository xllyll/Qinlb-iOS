//
//  XYAdScrollView.m
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "XYAdScrollView.h"
#import "UIImageView+XYImageView.h"


#define UISCREENWIDTH  self.bounds.size.width//广告的宽度
#define UISCREENHEIGHT  self.bounds.size.height//广告的高度

#define HIGHT self.bounds.origin.y //由于_pageControl是添加进父视图的,所以实际位置要参考,滚动视图的y坐标

static CGFloat const chageImageTime = 3.0;
static NSUInteger currentImage = 1;//记录中间图片的下标,开始总是为1

@interface XYAdScrollView ()

{
    //广告的label
    UILabel * _adLabel;
    //循环滚动的三个视图
    UIImageView * _leftImageView;
    UIImageView * _centerImageView;
    UIImageView * _rightImageView;
    //循环滚动的周期时间
    NSTimer * _moveTime;
    //用于确定滚动式由人导致的还是计时器到了,系统帮我们滚动的,YES,则为系统滚动,NO则为客户滚动(ps.在客户端中客户滚动一个广告后,这个广告的计时器要归0并重新计时)
    BOOL _isTimeUp;
    //为每一个图片添加一个广告语(可选)
    UILabel * _leftAdLabel;
    UILabel * _centerAdLabel;
    UILabel * _rightAdLabel;
    
    UIControl *_current_pageView;
    NSMutableArray *_pageViews;
}

@property (retain,nonatomic,readonly) UIImageView * leftImageView;
@property (retain,nonatomic,readonly) UIImageView * centerImageView;
@property (retain,nonatomic,readonly) UIImageView * rightImageView;

@end

@implementation XYAdScrollView

#pragma mark - 自由指定广告所占的frame
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        [self setup];
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [self setup];
}
-(void)setup{
    
    self.bounces = NO;
    
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
    self.pagingEnabled = YES;
    self.contentOffset = CGPointMake(UISCREENWIDTH, 0);
    self.contentSize = CGSizeMake(UISCREENWIDTH * 3, UISCREENHEIGHT);
    self.delegate = self;
    
    float hight = 50;
    
    _leftImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWIDTH, UISCREENHEIGHT)];
    UIImageView *imageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, _leftImageView.bounds.size.height-hight, _leftImageView.bounds.size.width, hight)];
//    imageView1.image =[UIImage imageNamed:@"sp_mr_img"];
    [_leftImageView addSubview:imageView1];
    [self addSubview:_leftImageView];
    
    
    _centerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(UISCREENWIDTH, 0, UISCREENWIDTH, UISCREENHEIGHT)];
    UIImageView *imageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, _leftImageView.bounds.size.height-hight, _leftImageView.bounds.size.width, hight)];
//    imageView2.image =[UIImage imageNamed:@"sp_mr_img"];
    [_centerImageView addSubview:imageView2];
    [self addSubview:_centerImageView];
    
    
    _rightImageView = [[UIImageView alloc]initWithFrame:CGRectMake(UISCREENWIDTH*2, 0, UISCREENWIDTH, UISCREENHEIGHT)];
    UIImageView *imageView3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, _leftImageView.bounds.size.height-hight, _leftImageView.bounds.size.width, hight)];
//    imageView3.image =[UIImage imageNamed:@"sp_mr_img"];
    [_rightImageView addSubview:imageView3];
    [self addSubview:_rightImageView];
    
    _moveTime = [NSTimer scheduledTimerWithTimeInterval:chageImageTime target:self selector:@selector(animalMoveImage) userInfo:nil repeats:YES];
    _isTimeUp = NO;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewCheck:)];
    tap1.numberOfTouchesRequired =1;
    _leftImageView.userInteractionEnabled = YES;
    [_leftImageView addGestureRecognizer:tap1];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewCheck:)];
    tap2.numberOfTouchesRequired =1;
    _centerImageView.userInteractionEnabled = YES;
    [_centerImageView addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewCheck:)];
    tap3.numberOfTouchesRequired =1;
    _rightImageView.userInteractionEnabled = YES;
    [_rightImageView addGestureRecognizer:tap3];
}
-(void)imageViewCheck:(UITapGestureRecognizer*)sender{
    if (_fudelegate) {
        [_fudelegate XYAdScrollViewSelect:_pageControl.currentPage];
    }
}
#pragma mark - 设置广告所使用的图片(名字)
- (void)setImageNameArray:(NSArray *)imageNameArray
{
    if (_imageDataArray==nil
        ) {
        _imageDataArray = [NSMutableArray array];
        for (NSString *s in imageNameArray) {
            [_imageDataArray addObject:s];
        }
    }
    if (_imageDataArray.count==1) {
        [_moveTime invalidate];
    }
    
    _imageNameArray = imageNameArray;
    
    [self setPageControlShowStyle:XYUIPageControlShowStyleCenter];
    _pageControl.hidden = YES;
    float p_v = 8;
    float sizw = 5;
    _pageViews = [NSMutableArray array];
    for (int i = 0; i < _imageNameArray.count; i++) {
        UIControl *page = [[UIControl alloc]initWithFrame:CGRectMake( i*(p_v+sizw)+(self.bounds.size.width/2-sizw*2-p_v*1.5), self.bounds.size.height-15, sizw, sizw)];
        page.backgroundColor  = [UIColor whiteColor];
        
        page.layer.cornerRadius = page.bounds.size.height/2.0;
        page.clipsToBounds = YES;
        
        [self.superview addSubview:page];
        if (i==0) {
            _current_pageView = page;
            _current_pageView.backgroundColor = [UIColor grayColor];
            _current_pageView.transform = CGAffineTransformScale(_current_pageView.transform, 1.5, 1.5);
        }
        [_pageViews addObject:page];
    }
    
    
    UIImage *image = [UIImage imageNamed:@"sp_mr_img"];
    
    [_leftImageView xy_setImageURL:_imageNameArray[0] placeholderImage:image];
    
    if (_imageNameArray.count==1) {
        [_centerImageView xy_setImageURL:_imageNameArray[0] placeholderImage:image];
        [_rightImageView xy_setImageURL:_imageNameArray[0] placeholderImage:image];
        return;
    }
    [_centerImageView xy_setImageURL:_imageNameArray[1] placeholderImage:image];
    if (_imageNameArray.count==2) {
        //[_rightImageView xy_setImageURL:_imageNameArray[1] placeholderImage:image];
        return;
    }
    [_rightImageView xy_setImageURL:_imageNameArray[2] placeholderImage:image];
    
    
//    UIImage *image1 = [FUImageUtils getImageName:_imageNameArray[0] foundation:K_F_Name];
//    if (image1==nil) {
//        image1= [UIImage imageNamed:@"loading2"];
//    }
    
//    NSURL *url = [NSURL URLWithString:_imageNameArray[0]];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        if (data) {
//            UIImage *image = [UIImage imageWithData:data];
//            _leftImageView.image = image;
//            [FUImageUtils saveImage:image imageName:_imageNameArray[0] foundation:K_F_Name];
//            [_imageDataArray insertObject:image atIndex:0];
//            [_imageDataArray removeObjectAtIndex:1];
//        }
//    }];
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//    UIImage *image2 = [FUImageUtils getImageName:_imageNameArray[1] foundation:K_F_Name];
//    if (image2==nil) {
//        image2= [UIImage imageNamed:@"loading2"];
//    }
//    _centerImageView.image = image2;
    
//    NSURL *url2 = [NSURL URLWithString:_imageNameArray[1]];
//    NSURLRequest *request2 = [NSURLRequest requestWithURL:url2];
//    [NSURLConnection sendAsynchronousRequest:request2 queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        if (data) {
//            UIImage *image = [UIImage imageWithData:data];
//            _centerImageView.image = image;
//            [FUImageUtils saveImage:image imageName:_imageNameArray[1] foundation:K_F_Name];
//            [_imageDataArray insertObject:image atIndex:1];
//            
//            [_imageDataArray removeObjectAtIndex:2];
//        }
//    }];
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//    UIImage *image3 = [FUImageUtils getImageName:_imageNameArray[2] foundation:K_F_Name];
//    if (image3==nil) {
//        image3= [UIImage imageNamed:@"loading2"];
//    }
//    _rightImageView.image = image3;
    
    
//    NSURL *url3 = [NSURL URLWithString:_imageNameArray[2]];
//    NSURLRequest *request3 = [NSURLRequest requestWithURL:url3];
//    [NSURLConnection sendAsynchronousRequest:request3 queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        if (data) {
//            UIImage *image = [UIImage imageWithData:data];
//            _rightImageView.image = image;
//            //[FUImageUtils saveImage:image imageName:_imageNameArray[2] foundation:K_F_Name];
//            [_imageDataArray insertObject:image atIndex:2];
//            [_imageDataArray removeObjectAtIndex:3];
//        }
//    }];
}

#pragma mark - 设置每个对应广告对应的广告语
- (void)setAdTitleArray:(NSArray *)adTitleArray withShowStyle:(XYAdTitleShowStyle)adTitleStyle
{
    _adTitleArray = adTitleArray;
    
    if(adTitleStyle == XYAdTitleShowStyleNone)
    {
        return;
    }
    
    if(_leftAdLabel==nil){
        _leftAdLabel = [[UILabel alloc]init];
    }
    if(_centerAdLabel==nil){
        _centerAdLabel = [[UILabel alloc]init];
    }
    if(_rightAdLabel==nil){
        _rightAdLabel = [[UILabel alloc]init];
    }
    
    _leftAdLabel.textColor =[UIColor whiteColor];
    _centerAdLabel.textColor = [UIColor whiteColor];
    _rightAdLabel.textColor = [UIColor whiteColor];
    [_leftAdLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    [_centerAdLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    [_rightAdLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    
    
    _leftAdLabel.frame = CGRectMake(10, UISCREENHEIGHT - 40, UISCREENWIDTH, 20);
    [_leftImageView addSubview:_leftAdLabel];
    _centerAdLabel.frame = CGRectMake(10, UISCREENHEIGHT - 40, UISCREENWIDTH, 20);
    [_centerImageView addSubview:_centerAdLabel];
    _rightAdLabel.frame = CGRectMake(10, UISCREENHEIGHT - 40, UISCREENWIDTH, 20);
    [_rightImageView addSubview:_rightAdLabel];
    
    if (adTitleStyle == XYAdTitleShowStyleLeft) {
        _leftAdLabel.textAlignment = NSTextAlignmentLeft;
        _centerAdLabel.textAlignment = NSTextAlignmentLeft;
        _rightAdLabel.textAlignment = NSTextAlignmentLeft;
    }
    else if (adTitleStyle == XYAdTitleShowStyleCenter)
    {
        _leftAdLabel.textAlignment = NSTextAlignmentCenter;
        _centerAdLabel.textAlignment = NSTextAlignmentCenter;
        _rightAdLabel.textAlignment = NSTextAlignmentCenter;
    }
    else
    {
        _leftAdLabel.textAlignment = NSTextAlignmentRight;
        _centerAdLabel.textAlignment = NSTextAlignmentRight;
        _rightAdLabel.textAlignment = NSTextAlignmentRight;
    }
    
    
    _leftAdLabel.text = _adTitleArray[0];
    _centerAdLabel.text = _adTitleArray[1];
    _rightAdLabel.text = _adTitleArray[2];
    
}


#pragma mark - 创建pageControl,指定其显示样式
- (void)setPageControlShowStyle:(XYUIPageControlShowStyle)PageControlShowStyle
{
    if (PageControlShowStyle == XYUIPageControlShowStyleNone) {
        return;
    }
    _pageControl = [[UIPageControl alloc]init];
    _pageControl.numberOfPages = _imageNameArray.count;
    
    if (PageControlShowStyle == XYUIPageControlShowStyleLeft)
    {
        _pageControl.frame = CGRectMake(10, HIGHT+UISCREENHEIGHT - 20, 20*_pageControl.numberOfPages, 20);
    }
    else if (PageControlShowStyle == XYUIPageControlShowStyleCenter)
    {
        _pageControl.frame = CGRectMake(0, 0, 20*_pageControl.numberOfPages, 20);
        _pageControl.center = CGPointMake(UISCREENWIDTH/2.0, HIGHT+UISCREENHEIGHT - 10);
    }
    else
    {
        _pageControl.frame = CGRectMake( UISCREENWIDTH - 20*_pageControl.numberOfPages, HIGHT+UISCREENHEIGHT - 20, 20*_pageControl.numberOfPages, 20);
    }
    _pageControl.currentPage = 0;
    
    _pageControl.enabled = NO;
    _pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    _pageControl.currentPageIndicatorTintColor = [UIColor grayColor];
    [self performSelector:@selector(addPageControl) withObject:nil afterDelay:0.1f];
}
//由于PageControl这个空间必须要添加在滚动视图的父视图上(添加在滚动视图上的话会随着图片滚动,而达不到效果)
- (void)addPageControl
{
    [[self superview] addSubview:_pageControl];
}

#pragma mark - 计时器到时,系统滚动图片
- (void)animalMoveImage
{
    
    [self setContentOffset:CGPointMake(UISCREENWIDTH * 2, 0) animated:YES];
    _isTimeUp = YES;
    [NSTimer scheduledTimerWithTimeInterval:0.4f target:self selector:@selector(scrollViewDidEndDecelerating:) userInfo:nil repeats:NO];
}

#pragma mark - 图片停止时,调用该函数使得滚动视图复用
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (_imageNameArray==nil) {
        return;
    }
    if (self.contentOffset.x == 0)
    {
        currentImage = (currentImage-1)%_imageNameArray.count;
        _pageControl.currentPage = (_pageControl.currentPage - 1)%_imageNameArray.count;
    }
    else if(self.contentOffset.x == UISCREENWIDTH * 2)
    {
        
        currentImage = (currentImage+1)%_imageNameArray.count;
        _pageControl.currentPage = (_pageControl.currentPage + 1)%_imageNameArray.count;
    }
    else
    {
        return;
    }
    _current_pageView.backgroundColor = [UIColor whiteColor];
    _current_pageView.transform = CGAffineTransformScale(_current_pageView.transform, 0.666666666667, 0.666666666667);
    UIControl *page = _pageViews[_pageControl.currentPage];
    page.backgroundColor = [UIColor grayColor];
    page.transform = CGAffineTransformScale(_current_pageView.transform, 1.5, 1.5);
    _current_pageView = page;
    
    UIImage *loadimage = [UIImage imageNamed:@"sp_mr_img"];
    
    
//    UIImage *image1 = _imageDataArray[(currentImage-1)%_imageNameArray.count];
//    if ([image1 isKindOfClass:[NSString class]]) {
//        image1 = [FUImageUtils getImageName:_imageNameArray[(currentImage-1)%_imageNameArray.count] foundation:K_F_Name];
//        if (image1==nil) {
//            image1 = [UIImage imageNamed:@"loading2"];
//        }
//        NSURL *url = [NSURL URLWithString:_imageNameArray[(currentImage-1)%_imageNameArray.count]];
//        NSURLRequest *request = [NSURLRequest requestWithURL:url];
//        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//            if (data) {
//                UIImage *image = [UIImage imageWithData:data];
//                [FUImageUtils saveImage:image imageName:_imageNameArray[(currentImage-1)%_imageNameArray.count] foundation:K_F_Name];
//                [_imageDataArray insertObject:image atIndex:(currentImage-1)%_imageNameArray.count];
//                [_imageDataArray removeObjectAtIndex:(currentImage-1)%_imageNameArray.count+1];
//            }
//        }];
//    }
    [_leftImageView xy_setImageURL:_imageNameArray[(currentImage-1)%_imageNameArray.count] placeholderImage:loadimage];
    _leftAdLabel.text = _adTitleArray[(currentImage-1)%_imageNameArray.count];
    
//    UIImage *image2 = _imageDataArray[(currentImage)%_imageNameArray.count];
//    if ([image2 isKindOfClass:[NSString class]]) {
//        image2 = [FUImageUtils getImageName:_imageNameArray[(currentImage)%_imageNameArray.count] foundation:K_F_Name];
//        if (image2==nil) {
//            image2 = [UIImage imageNamed:@"loading2"];
//        }
//        NSURL *url = [NSURL URLWithString:_imageNameArray[(currentImage)%_imageNameArray.count]];
//        NSURLRequest *request = [NSURLRequest requestWithURL:url];
//        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//            if (data) {
//                UIImage *image = [UIImage imageWithData:data];
//                [FUImageUtils saveImage:image imageName:_imageNameArray[(currentImage)%_imageNameArray.count] foundation:K_F_Name];
//                [_imageDataArray insertObject:image atIndex:(currentImage)%_imageNameArray.count];
//                [_imageDataArray removeObjectAtIndex:(currentImage)%_imageNameArray.count+1];
//            }
//        }];
//    }
//    _centerImageView.image = image2;
    
    [_centerImageView xy_setImageURL:_imageNameArray[(currentImage)%_imageNameArray.count] placeholderImage:loadimage];
    
    _centerAdLabel.text = _adTitleArray[currentImage%_imageNameArray.count];
//    UIImage *image3 = _imageDataArray[(currentImage+1)%_imageNameArray.count];
//    
//    if ([image3 isKindOfClass:[NSString class]]) {
//        image3 = [FUImageUtils getImageName:_imageNameArray[(currentImage+1)%_imageNameArray.count] foundation:K_F_Name];
//        if (image3==nil) {
//            image3 = [UIImage imageNamed:@"loading2"];
//        }
//        
//        NSURL *url = [NSURL URLWithString:_imageNameArray[(currentImage+1)%_imageNameArray.count]];
//        NSURLRequest *request = [NSURLRequest requestWithURL:url];
//        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//            if (data) {
//                UIImage *image = [UIImage imageWithData:data];
//                [FUImageUtils saveImage:image imageName:_imageNameArray[(currentImage+1)%_imageNameArray.count] foundation:K_F_Name];
//                [_imageDataArray insertObject:image atIndex:(currentImage+1)%_imageNameArray.count];
//                [_imageDataArray removeObjectAtIndex:(currentImage+1)%_imageNameArray.count+1];
//            }
//        }];
//    }
//    _rightImageView.image = image3;
    
    [_rightImageView xy_setImageURL:_imageNameArray[(currentImage+1)%_imageNameArray.count] placeholderImage:loadimage];
    _rightAdLabel.text = _adTitleArray[(currentImage+1)%_imageNameArray.count];
    
    self.contentOffset = CGPointMake(UISCREENWIDTH, 0);
    
    //手动控制图片滚动应该取消那个三秒的计时器
    if (!_isTimeUp) {
        [_moveTime setFireDate:[NSDate dateWithTimeIntervalSinceNow:chageImageTime]];
    }
    _isTimeUp = NO;
}

@end
