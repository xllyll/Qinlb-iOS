//
//  XYAUAdScrollView.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/12.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYAdScrollView.h"

@interface XYAUAdScrollView : UIScrollView<UIScrollViewDelegate>

@property (retain,nonatomic,readonly) UIPageControl * pageControl;
@property (retain,nonatomic,readwrite) NSArray * imageNameArray;
@property (retain,nonatomic,readonly) NSArray * adTitleArray;

@property (retain,nonatomic,readonly) NSMutableArray *imageDataArray;

@property (weak , nonatomic)id<XYAdScrollViewDelegate>fudelegate;

@property (assign,nonatomic,readwrite) XYUIPageControlShowStyle  PageControlShowStyle;
@property (assign,nonatomic,readonly) XYAdTitleShowStyle  adTitleStyle;

- (void)setAdTitleArray:(NSArray *)adTitleArray withShowStyle:(XYAdTitleShowStyle)adTitleStyle;


@end
