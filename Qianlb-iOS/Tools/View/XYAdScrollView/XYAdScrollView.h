//
//  XYAdScrollView.h
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/8.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, XYUIPageControlShowStyle)
{
    XYUIPageControlShowStyleNone,//default
    XYUIPageControlShowStyleLeft,
    XYUIPageControlShowStyleCenter,
    XYUIPageControlShowStyleRight,
};

typedef NS_ENUM(NSUInteger, XYAdTitleShowStyle)
{
    XYAdTitleShowStyleNone,
    XYAdTitleShowStyleLeft,
    XYAdTitleShowStyleCenter,
    XYAdTitleShowStyleRight,
};

@protocol XYAdScrollViewDelegate <NSObject>

-(void)XYAdScrollViewSelect:(NSInteger)index;

@end

@interface XYAdScrollView : UIScrollView<UIScrollViewDelegate>

@property (retain,nonatomic,readonly) UIPageControl * pageControl;
@property (retain,nonatomic,readwrite) NSArray * imageNameArray;
@property (retain,nonatomic,readonly) NSArray * adTitleArray;

@property (retain,nonatomic,readonly) NSMutableArray *imageDataArray;

@property (weak , nonatomic)id<XYAdScrollViewDelegate>fudelegate;

@property (assign,nonatomic,readwrite) XYUIPageControlShowStyle  PageControlShowStyle;
@property (assign,nonatomic,readonly) XYAdTitleShowStyle  adTitleStyle;

- (void)setAdTitleArray:(NSArray *)adTitleArray withShowStyle:(XYAdTitleShowStyle)adTitleStyle;

@end
