//
//  XYAUAdScrollView.m
//  Gogbuy-iOS
//
//  Created by 杨卢银 on 16/9/12.
//  Copyright © 2016年 杨卢银. All rights reserved.
//

#import "XYAUAdScrollView.h"


@interface XYAUAdScrollView ()
{
    //广告的label
    UILabel * _adLabel;
    //为每一个图片添加一个广告语(可选)
    UILabel * _leftAdLabel;
    UILabel * _centerAdLabel;
    UILabel * _rightAdLabel;
    
    //循环滚动的周期时间
    NSTimer * _moveTime;
    //用于确定滚动式由人导致的还是计时器到了,系统帮我们滚动的,YES,则为系统滚动,NO则为客户滚动(ps.在客户端中客户滚动一个广告后,这个广告的计时器要归0并重新计时)
    BOOL _isTimeUp;
    
    UIControl *_current_pageView;
    NSMutableArray *_pageViews;
}
@property (strong,nonatomic) UIImageView * leftImageView;
@property (strong,nonatomic) UIImageView * centerImageView;
@property (strong,nonatomic) UIImageView * rightImageView;



@end

@implementation XYAUAdScrollView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setAdTitleArray:(NSArray *)adTitleArray withShowStyle:(XYAdTitleShowStyle)adTitleStyle{
    
}
@end
